
## 1. Einführung in das POLE-Datenmodell
- **Definition**: POLE steht für Personen, Objekte, Lokationen und Ereignisse.
- **Einsatzgebiet**: Anwendung in polizeilichen und Kinderschutzuntersuchungen.
- **Ziel**: Vernetzte Daten analysieren, um Zusammenhänge zu verstehen und präventiv tätig zu werden.



\pagebreak

## 2. Anwendungsfälle des POLE-Datenmodells
- **Typische Use Cases**:
  - Polizeiarbeit
  - Terrorismusbekämpfung
  - Kinderschutz und Sozialdienste
  - Ermittlungen bei Versicherungsbetrug
- **Vorteile**:
  - Echtzeit-Datenverarbeitung für proaktive Entscheidungen.
  - Erkennung von Verhaltensmustern 
  - Gezieltere Ressourcenplanung
- Graphen eignen sich perfekt für diese Anwendungsfälle



\pagebreak

## 3. Über die Daten
- **Datenquellen**: Öffentliche Kriminalitätsdaten (z. B. data.gov.uk).
- **Datenumfang**: Kriminalitäts- und Standortdaten, keine personenbezogenen Daten.
- **Anwendungsbeispiel**: Straßendaten aus Manchester, UK (August 2017).
- **Datencharakteristik**:
  - Längen-/Breitengrade, Verbrechensarten, Postleitzahlen.
  - Fiktive Szenarien und zufällig generierte Zusatzdaten.



\pagebreak

## 4. General Queries - Schema
- Darstellung des Graphschemas:
  - Knoten: Personen, Orte, Ereignisse.
  - Kanten: Beziehungen wie **KNOWS**, **FAMILY_REL**, **KNOWS_LW**.
- Verknüpfung von Standorten mit Postleitzahlen und Gebieten, um Abfragen zu verfeinern.



\pagebreak

## 5. Häufigste Verbrechen
- **Cypher-Abfrage**:
  ```cypher
  MATCH (c:Crime)
  RETURN c.type AS crime_type, count(c) AS total
  ORDER BY count(c) DESC;
  ```
- **Erkenntnis**:
  - Gewalt- und Sexualdelikte am häufigsten.
  - Waffendelikte am seltensten.



\pagebreak

## 6. Queries - Lokale Kriminalitätsschwerpunkte
- Identifikation von Orten mit hoher Kriminalitätsrate:
  - Beispiel: Piccadilly in der Nähe des Bahnhofs, Einkaufszentren.
- Cypher-Abfrage für Top-Standorte:
  ```cypher
  MATCH (l:Location)<-[:OCCURRED_AT]-(:Crime)
  RETURN l.address AS address, count(l) AS total
  ORDER BY count(l) DESC LIMIT 15;
  ```


\pagebreak

## 7. Räumliches Umfeld - Verbrechen in der Nähe einer Adresse
- Suche nach Verbrechen im Umkreis von 500 Metern um eine Adresse:
  - Beispiel: "1 Coronation Street" in Manchester.
- Verwendung der **point.distance**-Funktion in Cypher.

\pagebreak

## 8. Personal - Verbrechen, die von Inspektor Morse untersucht wurden
- Analyse von Verbrechen, die einem spezifischen Ermittler zugewiesen sind:
  - Verbindung von Verbrechen mit Ermittlerknoten.
  - Visualisierung der Verbindungen im Graph.

\pagebreak

## 9. Personal - Verbrechen, die von Officer Larive untersucht werden
- Untersuchung laufender Ermittlungen zu Drogendelikten.
- Beispiele:
  - Zwei Fälle von Cannabisbesitz.
  - Ein Fall von Cannabisproduktion.

\pagebreak

## 10. Verbindungen - Kürzeste Pfade zwischen Personen
- Cypher-Abfrage zur Analyse von Beziehungen zwischen Verdächtigen:
  ```cypher
  MATCH path = allshortestpaths((p1)-[:KNOWS*..3]-(p2))
  RETURN path;
  ```
- Ziel: Verbindungen zwischen Personen in sozialen Netzwerken aufdecken.

\pagebreak

## 11. Soziale Netzwerke - Personen mit Verbindungen zu Drogenverbrechen
- Analyse von sozialen Netzwerken im Kontext von Drogendelikten.
- Identifikation von Clustern und potenziellen kriminellen Netzwerken.

\pagebreak

## 12. Untersuchung zu schutzbedürftigen Personen - Top 5
- Definition: Gefährdete Personen kennen viele Freunde, die mit Verbrechen in Verbindung stehen.
- Cypher-Abfrage zur Identifikation der Top 5 gefährdeten Personen.

\pagebreak

## 13. Untersuchung zu schutzbedürftigen Personen - Friend of a Friend
- Erweiterte Analyse: Freunde von Freunden mit Verbrechensverbindungen.
- Ziel: Verständnis komplexer sozialer Netzwerke.

\pagebreak

## 14. Untersuchung zu schutzbedürftigen Personen - Graph-Visualisierung
- Detaillierte Exploration der sozialen Beziehungen gefährdeter Personen.
- Wir sehen, dass Anne Freeman 8 gefährliche Freunde hat. Mit ihrer ID zeigt uns diese Abfrage den Graphen dieser Freunde, in dem wir navigieren und es erkunden können.

\pagebreak

## 15. Auf der Suche nach gefährlichen Freunden vor Ort
- Identifikation von gefährlichen Freunden, die in der Nähe wohnen.
- Analyse regionaler Risiken durch soziale Verbindungen.
- Wir sehen, dass nur ihr Freund Craig, den sie über soziale Netzwerke kennt, im selben Gebiet (SK1) wie Anne lebt. Craig wurde mit zwei Verstößen gegen die öffentliche Ordnung in Verbindung gebracht.

\pagebreak

## 16. Verbindungen zwischen gefährdeten Personen
- Untersuchung direkter und indirekter Verbindungen zwischen gefährdeten Personen.
- Ziel: Aufdecken gemeinsamer Netzwerke.
- Es hat sich herausgestellt, dass es Verbindungen zwischen ihnen gibt, die unterschiedlich lang sind. Es gibt tatsächlich mehrere Wege, über die einige von ihnen miteinander verbunden sind.

\pagebreak

## 17. Auf der Suche nach gefährlichen Freunden der Familie
- Fokus auf familiäre Beziehungen zu gefährlichen Freunden.
- Kombination von sozialen und familiären Beziehungen zur Risikoanalyse.
- Sie sollten 5 Personen sehen, die Familienmitglieder mit gefährlichen Freunden haben.

\pagebreak

## 18. Auf der Suche nach gefährlichen Freunden der Familie
- Zusammenleben mit gefährlichen Verwandten ?
- Projektion sozialer Netzwerke zur Optimierung von Berechnungen.
- Diese Abfrage gibt nur 2 Personen zurück, aber die Person mit der höchsten Anzahl an gefährlichen Familienfreunden (Kimberly Alexander) ist dieselbe wie in den Ergebnissen der vorherigen Abfrage.

\pagebreak

## 19. Kimberlys Graph
- In Kimberleys Graphen sehen wir, dass Kimberly (12 Jahre alt) mit ihrer Mutter Bonnie in 53 Ridge Grove lebt. 
- Bonnie hat mehrere Freunde, die mit einer Reihe von Verbrechen unterschiedlicher Art in Verbindung stehen. 
- Es besteht eine hohe Wahrscheinlichkeit, dass Kimberly Umgang mit diesen Menschen hat und sie deshlab potenziell gefährdet ist.

\pagebreak

## 20. Graph Algorithmen - Graph Projection
- Nutzung von Graphprojektionen zur In-Memory-Analyse.
- Projektion sozialer Netzwerke zur Optimierung von Berechnungen.
- Personenknoten und KNOWS-Beziehungen im In-Memory-Graphen projizieren.

\pagebreak

## 21. Algorithmen - Triangle Count
- Identifikation von Dreiecken (A kennt B, B kennt C, C kennt A).
- Anwendung: Analyse von Clustern und sozialen Gruppen.

\pagebreak

## 22. Algorithmen - Triangle Count
- Wir können uns den Graph für einen der zurückgegebenen Dreieckssätze ansehen – Deborah Ford, die zu zehn Dreiecken gehört.
- Wir sehen, dass Patricia Carr sowohl Deborah Ford als auch Jonathan Hunt kennt und dass sowohl Deborah als auch Jonathan Peter Bryant, Harry Lopez und Phillip Perry kennen. 
- Daraus können wir schließen, dass Patricia auch Peter, Harry und Phillip kennt.

\pagebreak

## 23. Algorithmen - Triangle Count on a Subgraph
- Begrenzung der Analyse auf Teilgraphen (z. B. kriminelle Netzwerke).
- Cypher-Abfrage zur Teilnetzprojektion.

\pagebreak

## 24. Algorithmen - Triangle Count on a Subgraph
- Dreiecke, die mit einem der Top-Ergebnisse der vorherigen Abfrage (Phillip Williamson) verbunden sind, zeigen eine Gruppe von Personen, die sich kennen, miteinander verwandt sind und/oder zusammenleben.  
- Die Namen sind aus einer vorherigen Drogenuntersuchung **bekannt**.  
- Es handelt sich möglicherweise um eine Gruppe potenzieller Krimineller.  
- Neben Drogendelikten gibt es viele Fahrzeugdelikte, die mit dieser sozialen Gruppe in Verbindung stehen.  
- Mögliche Spezialisierung der Gruppe auf Autodiebstahl.  
- Die Algorithmen haben automatisch Verbindungen aufgedeckt, die früher gezielt gesucht werden mussten.  

\pagebreak

## 25. Algorithmen - Betweenness Centrality
- Messung der Zentralität von Knoten (wichtigste Verbindungen im Graph über KENNT-Kanten).
- Die Konten befinden sich auf dem **kürzesten Weg** zwischen den meisten anderen Personen über die „KENNT“-Beziehung (wobei die Richtung der Beziehung ignoriert wird, da sie hier nicht sehr wichtig ist).
- Ziel: Identifikation von Schlüsselpersonen.

\pagebreak

## 26. Algorithmen
- Visualisierung des Graphen des besten Treffers (Annie Duncan) mit bis zu drei Ebenen.  
- Ziel: Untersuchung ihrer Vernetzung.  
- Annie Duncan sitzt zwischen mehreren Clustern/Gemeinschaften des Graphen.  
- Weitere Ebenen liefern mehr Ergebnisse.  
- Visualisierung weiterer Ebenen ist schwieriger und zeitaufwendiger.  

\pagebreak

## 27. Ende
- Zusammenfassung: Vorteile der POLE-Datenmodellierung.
- Praxisrelevanz: Komplexere Szenarien erfordern weitere Anpassungen.
- Sicherheit und Datenintegrität sind entscheidend.
- Einige Beispiele für zusätzliche Komplexität in in einem realen Szenario wären:
    - Verwendung von „Personas“ anstelle von „Personen“, um Dinge wie Aliasnamen zu berücksichtigen.
    - Mehr Typen von Beziehungen zwischen Personen und Straftaten (z.B. Zeuge, Opfer, Verdächtiger, Verurteilter)
    - Rückverfolgbarkeit und Prüfung von Daten: In der Praxis ist es sehr wichtig, die Herkunft der Daten zu klären und nachzuweisen, dass wir das Recht haben, diese Informationen zu speichern (d. h. wurden sie im Rahmen einer Untersuchung entdeckt, sind sie öffentlich zugänglich, sind sie Teil eines anderen Dokuments oder stehen sie in Zusammenhang mit einem anderen Fall, wer hat die Informationen wann eingegeben, wer hat sie aktualisiert, wurden sie überprüft usw.).
    - Hinzufügen eines robusten Sicherheitsmechanismus, um den Zugriff auf Daten auf diejenigen zu beschränken, die über die entsprechende Berechtigung verfügen.
    - Verwendung von Gewichtungen für Queries und Algorithmen – zum Beispiel könnten einige Straftaten als gefährlicher angesehen werden als andere (d. h. Gewalt- und Sexualdelikte sind schwerwiegender als Ladendiebstahl), oder einige Beziehungen könnten als zuverlässiger oder enger angesehen werden (d. h. „Familie“ oder „lebt mit“ könnten stärker gewichtet werden als „soziales Netzwerk“)




