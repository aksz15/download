# riOpac2neo4j

## cypher-Importskript

Im Hauptverzeichnis findet sich das Import-Skript für die Psql-csv-Exporte

## Postgresql

Auf einem Debian-Linux wird zunächst wird der Psql-Dump importiert. 

Als Nutzer postgres:

`psql -d opac < 2023-06-08-opac.sql`

Anschließend mit psql folgende Befehle ausführen:

```
COPY (SELECT * FROM werke) TO '/tmp/werke.csv' WITH CSV header;
COPY (SELECT * FROM werke WHERE objektart = 1) TO '/tmp/werke1.csv' WITH CSV header;
COPY (SELECT * FROM werke WHERE objektart = 2) TO '/tmp/werke2.csv' WITH CSV header;
COPY (SELECT * FROM werke WHERE objektart = 3) TO '/tmp/werke3.csv' WITH CSV header;
COPY (SELECT * FROM werke WHERE objektart = 4) TO '/tmp/werke4.csv' WITH CSV header;
COPY (SELECT * FROM werke WHERE objektart = 5) TO '/tmp/werke5.csv' WITH CSV header;
COPY (SELECT * FROM werke WHERE objektart = 6) TO '/tmp/werke6.csv' WITH CSV header;
COPY (SELECT * FROM werke WHERE objektart = 7) TO '/tmp/werke7.csv' WITH CSV header;
COPY (SELECT * FROM werke WHERE objektart = 8) TO '/tmp/werke8.csv' WITH CSV header;
COPY (SELECT * FROM sys_thesaurus2) TO '/tmp/systhes2.csv' WITH CSV header;
COPY (SELECT * FROM deskriptoren) TO '/tmp/deskriptoren.csv' WITH CSV header;
COPY (SELECT * FROM ejahr) TO '/tmp/ejahr.csv' WITH CSV header;
COPY (SELECT * FROM index_gesamttitel) TO '/tmp/index_gesamttitel.csv' WITH CSV header;
COPY (SELECT * FROM index_reihe) TO '/tmp/index_reihe.csv' WITH CSV header;
COPY (SELECT * FROM index_sammelwerk) TO '/tmp/index_sammelwerk.csv' WITH CSV header;
COPY (SELECT * FROM index_zeitschrift) TO '/tmp/index_zeitschrift.csv' WITH CSV header;
COPY (SELECT * FROM kurztitel) TO '/tmp/kurztitel.csv' WITH CSV header;
COPY (SELECT * FROM links_gesamttitel) TO '/tmp/links_gesamttitel.csv' WITH CSV header;
COPY (SELECT * FROM links_sammelwerk) TO '/tmp/links_sammelwerk.csv' WITH CSV header;
COPY (SELECT * FROM links_reihe) TO '/tmp/links_reihe.csv' WITH CSV header;
COPY (SELECT * FROM links_zeitschrift) TO '/tmp/links_zeitschrift.csv' WITH CSV header;
COPY (SELECT * FROM register_kurztitel) TO '/tmp/register_kurztitel.csv' WITH CSV header;
COPY (SELECT * FROM register_personen) TO '/tmp/register_personen.csv' WITH CSV header;
COPY (SELECT * FROM register_personen_links) TO '/tmp/register_personen_links.csv' WITH CSV header;
```

Im Ordner /tmp finden sich nun die *.csv-Dateien für die weitere Verarbeitung.
Hier im Repo befinden sich die Dateien im Ordner data.
